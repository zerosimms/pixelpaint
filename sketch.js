var mainDrawArea;
var pixelSize;
var gridSize;
var optionPen = true;
var canvasWidth = 640;
var canvasHeight = 640;
var canvasXPosition;
var canvasYPosition;
var offsetX = 0;
var grid = true;
var gridImage;
var spriteSheet;
var cellCount;
var spriteSheetVisibile = false;
var selectingArea = false;
var highlight;
var selectedArea;
var stampImage;
var mousePointer;
var optionStamp = false;
var optionCut = false;
var historyCounter = 1;
var undoNumber = 0;
var redoNumber = 0;
var undoing = false;
var redoing = false;

function setup() {
    mainDrawArea = createCanvas(canvasWidth, canvasHeight, P2D);
    mainDrawArea.parent('canvasArea');
    canvasXPosition = (windowWidth - width) / 2;
    canvasYPosition = (windowHeight - height) / 2;
    mainDrawArea.position(canvasXPosition, canvasYPosition);
    exportImage = createGraphics(canvasWidth, canvasHeight);
    stampImage = createGraphics(canvasWidth, canvasHeight);
    gridImage = createGraphics(canvasWidth, canvasHeight);
    spriteSheet = createGraphics(canvasWidth, canvasHeight);
    highlight = createGraphics(canvasWidth, canvasHeight);
    selectedArea = createGraphics(canvasWidth, canvasHeight);

    pixelSize = width / pixelSizeField.value;
    mousePointer = createGraphics(pixelSize, pixelSize);

    gridSize = width / gridSizeField.value;
    buttonStateOn('drawButton');
    optionPen = true;
    reDrawGrid();
    saveHistory(0);
    lastStateNumber = 1;
}

function draw() {
    listening();
}   