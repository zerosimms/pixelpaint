function gotFile(file)   {
    var img = loadImage(file.data);
    pixelate(img);
}

function pixelate(pixelImage){
    pixelImage.loadPixels();
    for (var y = 0; y < height; y += pixelSize) {
        for (var x = 0; x < width; x += pixelSize) {
            var offset = ((y * width) + x) * 10;
            fill(pixelImage.pixels[offset],
                pixelImage.pixels[offset + 1],
                pixelImage.pixels[offset + 2]);
            rect(x, y, pixelSize, pixelSize);
        }
    }
    pixelImage.updatePixels();
}