var optionColourSelecting = false;
var colourSelected = false;
var selectedColour = false;
var optionFlooding = false;
var floodAreaSelected = false;
var optionEraser = false;
var optionPen = true;
var originalMouseX;
var originalMouseY;
var distanceXShape;
var distanceYShape;
var spritesheetSelected = false;
var highlighterSelected = false;
var optionStamp = false;
var historyImage = [];
var lastStateNumber = 0;
var selectWidth = 0;
var selectHeight = 0;

function highlightedColour() {
    if (optionColourSelecting) {
        var colorInput = document.getElementById('colorBar');
        selectedColour = (get(mouseX, mouseY));
        fillColour = (rgbToHex(selectedColour[0], selectedColour[1], selectedColour[2]));
        colorInput.value = "#" + fillColour;
        selectedColour = colorInput.value;
        setTimeout(highlightedColour, 100);
    }
}

function floodFillCanvas() {
    if (mouseX < width && optionFlooding) {
        var colorInput = document.getElementById('colorBar');
        canvasExport = exportImage.drawingContext;
        canvasExport.fillStyle = colorInput.value;
        //The *2 hack is to flood the selected area on high res monitors
        canvasExport.fillFlood(round(mouseX) * 2, round(mouseY) * 2, 0, 0, 0, canvasWidth * 2, canvasHeight * 2);
        canvas = mainDrawArea.drawingContext;
        canvas.fillStyle = colorInput.value;
        canvas.fillFlood(round(mouseX) * 2, round(mouseY) * 2, 0, 0, 0, canvasWidth * 2, canvasHeight * 2);
    }
}

function mouseMoved() {
    if (highlighterSelected) {
        console.log("Highlighting");
        highlight.clear();
        multiX = mouseX - (mouseX % pixelSize);
        multiY = mouseY - (mouseY % pixelSize);
        distanceXShape = dist(originalMouseX, multiX, multiX, multiX);
        distanceYShape = dist(originalMouseY, multiY, multiY, multiY);
        highlight.clear();
        highlight.fill(0, 0, 0, 50);
        highlight.noStroke();
        highlight.rect(originalMouseX, originalMouseY, distanceXShape, distanceYShape);
    }
}

function stampMode() {
    console.log("Stamp Selected");
    optionStamp = true;
}

function cutMode() {
    console.log("Cut Selected");
    optionCut = true;
}

function loadHistory(i) {
    exportImage.clear();
    redrawNicely(i);
    console.log("image: " + i + " load");
}

function saveHistory(i) {
    // historyImage[i] = createGraphics(canvasWidth, canvasHeight);
    historyImage[i] = exportImage.get(0, 0, width, height);
}

function redrawNicely(i) {
    for (var x = 0; x < width; x += pixelSize) {
        for (var y = 0; y < height; y += pixelSize) {
            var colour = historyImage[i].get(x + 1, y + 1);
            exportImage.fill(colour);
            exportImage.rect(x, y, pixelSize, pixelSize);
        }
    }
}