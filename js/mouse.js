var clicked = false;

function mouseReleaesed() {
    clicked = false;
}

function mouseClicked() {
    var capturer = new CCapture({ format: 'gif', workersPath: 'js/helpers/', verbose: false, framerate: 10 });
    const undoButton = document.getElementById('undoButton');
    const redoButton = document.getElementById('redoButton');
    const stampButton = document.getElementById('stampButton');
    const cutButton = document.getElementById('cutButton');
    const highlightButton = document.getElementById('highlightButton');
    const spriteSheetButton = document.getElementById('spriteSheetButton');
    const spritesheetField = document.getElementById('spritesheetField');
    const hardlightButton = document.getElementById('hardlightButton');
    const overlayButton = document.getElementById('overlayButton');
    const darkenButton = document.getElementById('darkenButton');
    const penButton = document.getElementById('drawButton');
    const colourPickerButton = document.getElementById('colourPickerButton');
    const eraserButton = document.getElementById('eraserButton');
    const fileNameField = document.getElementById('fileNameField');
    const exportImageButton = document.getElementById('exportImageButton');
    const exportImageButtonResize = document.getElementById('exportImageButtonResize');
    const clearButton = document.getElementById('clearButton');
    const videoStartButton = document.getElementById('videoStartButton');
    const canvasWidthField = document.getElementById('canvasWidth');
    const canvasHeightField = document.getElementById('canvasHeight');
    const updateCanvasButton = document.getElementById('updateCanvas');
    const saveButton = document.getElementById('saveButton');
    const quickSaveButton = document.getElementById('quickSaveButton');
    const quickLoadButton = document.getElementById('quickLoadButton');
    const exportWidth = document.getElementById('exportWidth');
    const exportHeight = document.getElementById('exportHeight');
    const captureFrameButton = document.getElementById('captureFrameButton');
    const exportGifButton = document.getElementById('exportGifButton');

    if (!clicked) {
        undoButton.addEventListener('click', () => {
            if (lastStateNumber >= 1) {
                lastStateNumber--;
                print(lastStateNumber);
                loadHistory(lastStateNumber - 1);
            }
        });

        redoButton.addEventListener('click', () => {
            if (lastStateNumber < historyImage.length) {
                loadHistory(lastStateNumber);
                lastStateNumber++;
            }
        });

        highlightButton.addEventListener('click', () => {
            selectStatus();
            if (!highlighterSelected) {
                highlighterSelected = true;
                highlightButton.checked = true;
                buttonStateOn('highlightButton');
                originalMouseX = mouseX - (mouseX % pixelSize);
                originalMouseY = mouseY - (mouseY % pixelSize);
            }
            else if (highlighterSelected) {
                stampImage = exportImage.get(originalMouseX, originalMouseY, distanceXShape, distanceYShape);
                highlighterSelected = false;
                highlightButton.checked = false;
                selectingArea = false;
                selectedArea = false
                highlight.clear();
                buttonStateOff('highlightButton');
            }
        });

        cutButton.addEventListener('click', () => {
            cutStatus();
            if (!optionCut) {
                optionCut = true;
                cutButton.checked = true;
                buttonStateOn('cutButton');
                cutMode();
            } else {
                optionCut = false;
                buttonStateOff('cutButton');
            }
        });

        stampButton.addEventListener('click', () => {
            stampStatus();
            if (!optionStamp) {
                optionStamp = true;
                stampButton.checked = true;
                buttonStateOn('stampButton');
                stampMode();
            } else {
                optionStamp = false;
                buttonStateOff('stampButton');
            }
        });

        var frameCount = 1;
        gridSizeButton.addEventListener('click', () => {
            reDrawGrid();
        });

        spritesheetField.addEventListener('click', () => {
            if (!spriteSheetVisibile) {
                console.log("Sprite sheet cells: " + cellCount);
                spriteSheetVisibile = true;
                spriteSheetButton.checked = true;
                buttonStateOn('spriteSheetButton');
                drawSpritesheetTemplate();
            } else {
                spriteSheetVisibile = false;
                spriteSheetButton.checked = false;
                buttonStateOff('spriteSheetButton');
                spriteSheet.clear();
            }
        });

        spriteSheetButton.addEventListener('click', () => {
            spriteSheetSize = spritesheetField.value;
            console.log('Spritesheet size selected: ' + spriteSheetSize);
            drawSpritesheetTemplate();
        });

        hardlightButton.addEventListener('click', () => {
            if (!hardlightSelected) {
                hardlightSelected = true;
                hardlightButton.checked = true;
                buttonStateOn('hardlightButton');
            } else {
                hardlightSelected = false;
                hardlightButton.checked = false;
                buttonStateOff('hardlightButton');
            }
        });

        overlayButton.addEventListener('click', () => {
            if (!overlayFilterSelected) {
                overlayFilterSelected = true;
                overlayButton.checked = true;
                buttonStateOn('overlayButton');
            } else {
                overlayFilterSelected = false;
                overlayButton.checked = false;
                buttonStateOff('overlayButton');
            }
        });

        darkenButton.addEventListener('click', () => {
            if (!darkenFilterSelected) {
                darkenFilterSelected = true;
                darkenButton.checked = true;
                buttonStateOn('darkenButton');
            } else {
                darkenFilterSelected = false;
                darkenButton.checked = false;
                buttonStateOff('darkenButton');
            }
        });

        penButton.addEventListener('click', () => {
            if (!optionPen) {
                optionPen = true;
                penStatus();
            } else {
                optionPen = false;
                penStatus();
            }
        });

        eraserButton.addEventListener('click', () => {
            if (!optionEraser) {
                optionEraser = true;
                eraserStatus();
            } else {
                optionEraser = false;
                eraserStatus();
            }
        });

        floodButton.addEventListener('click', () => {
            if (!optionFlooding) {
                optionFlooding = true;
                floodStatus();
            } else {
                optionFlooding = false;
                floodStatus();
            }
        });

        colourPickerButton.addEventListener('click', () => {
            if (!optionColourSelecting) {
                optionColourSelecting = true;
                colourPickerStatus();
            } else {
                optionColourSelecting = false;
                colourPickerStatus();
            }
        });

        videoStartButton.addEventListener('click', () => {
            if (!videoPlay) {
                if (confirm("THIS WILL DELETE YOUR DRAWING, ARE YOU SURE?")) {
                    console.log("Start Video Recording");
                    startCamera();
                    videoPlay = true;
                    buttonStateOn('videoStartButton');
                } else {
                    videoStartButton.checked = false;
                    e.preventDefault();
                    buttonStateOff();
                }
            }
            else if (videoPlay) {
                stopCamera();
                videoPlay = false;
            }
        });

        captureFrameButton.addEventListener('click', () => {
            console.log("Adding Frame");
            capturer.start();
            capturer.capture(exportImage.canvas);
            document.getElementById("gifToast").innerHTML = "FRAME COUNT: " + frameCount;
            frameCount++;
            gifToast();
        });

        exportGifButton.addEventListener('click', () => {
            console.log("Creating Gif");
            capturer.save();
            capturer.stop();
            frameCount = 1;
        });

        exportImageButton.addEventListener('click', () => {
            console.log("Export Image");
            resizedImage = createGraphics(exportWidth.value / 2, exportHeight.value / 2);
            resizedImage.image(exportImage, 0, 0, resizedImage.width, resizedImage.height);
            save(resizedImage, 'export.png');
        });

        exportImageButtonResize.addEventListener('click', () => {
            console.log("Export Image");
            resizedImage = createGraphics(exportWidth.value / 2, exportHeight.value / 2);
            resizedImage.image(exportImage, 0, 0, resizedImage.width, resizedImage.height);
            save(resizedImage, 'export.png');
        });

        saveButton.addEventListener('click', () => {
            console.log("Save Image");
            savePixelImage(fileNameField.value);
        });

        quickSaveButton.addEventListener('click', () => {
            console.log("Quick Save Image");
            quickSave(fileNameField.value);
        });

        quickLoadButton.addEventListener('click', () => {
            console.log("Quick Load Image");
            loadState();
        });

        clearButton.addEventListener('click', () => {
            if (confirm("THIS WILL DELETE YOUR DRAWING, ARE YOU SURE?")) {
                console.log("Cleared Screen");
                resizeGrid(canvasWidth, canvasHeight);
            } else {
                e.preventDefault();
            }
        });

        updateCanvasButton.addEventListener('click', () => {
            if (confirm("THIS WILL DELETE YOUR DRAWING, ARE YOU SURE?")) {
                pixelSize = pixelSizeField.value;
                canvasWidth = canvasWidthField.value;
                canvasHeight = canvasHeightField.value;
                document.getElementById('exportHeight').value = canvasHeight
                document.getElementById('exportWidth').value = canvasWidth
                resizeGrid(canvasWidth, canvasHeight);
                console.log("Grid Size: " + pixelSize + " x " + pixelSize)
            } else {
                e.preventDefault();
            }
        });
        clicked = true;
    }
}
