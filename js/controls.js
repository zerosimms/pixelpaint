function penStatus() {
    if (optionPen) {
        buttonStateOn('drawButton');
    } else {
        buttonStateOff('drawButton');
    }
    buttonStateOff('eraserButton');
    optionEraser = false;
    buttonStateOff('floodButton');
    optionFlooding = false;
    buttonStateOff('colourPickerButton');
    optionColourSelecting = false;
    buttonStateOff('stampButton');
    optionStamp = false;
    buttonStateOff('cutButton');
    optionCut = false;
}

function eraserStatus() {
    if (optionEraser) {
        buttonStateOn('eraserButton');
    } else {
        buttonStateOff('eraserButton');
    }
    buttonStateOff('drawButton');
    optionPen = false;
    buttonStateOff('floodButton');
    optionFlooding = false;
    buttonStateOff('colourPickerButton');
    optionColourSelecting = false;
    buttonStateOff('stampButton');
    optionStamp = false;
}

function floodStatus() {
    if (optionFlooding) {
        buttonStateOn('floodButton');
    } else {
        buttonStateOff('floodButton');
    }
    buttonStateOff('drawButton');
    optionPen = false;
    buttonStateOff('eraserButton');
    optionEraser = false;
    buttonStateOff('colourPickerButton');
    optionColourSelecting = false;
    buttonStateOff('stampButton');
    optionStamp = false;
}

function colourPickerStatus() {
    if (optionColourSelecting) {
        highlightedColour();
        buttonStateOn('colourPickerButton');
    } else {
        buttonStateOff('colourPickerButton');
    }
    buttonStateOff('drawButton');
    optionPen = false;
    buttonStateOff('eraserButton');
    optionEraser = false;
    buttonStateOff('floodButton');
    optionFlooding = false;
    buttonStateOff('stampButton');
    optionStamp = false;
}

function selectStatus() {
    buttonStateOff('drawButton');
    optionPen = false;
    buttonStateOff('eraserButton');
    optionEraser = false;
    buttonStateOff('colourPickerButton');
    optionColourSelecting = false;
    buttonStateOff('floodButton');
    optionFlooding = false;
    buttonStateOff('stampButton');
    optionStamp = false;
    buttonStateOff('cutButton');
    optionCut = false;
    optionFlooding = false;
}

function stampStatus() {
    buttonStateOff('drawButton');
    optionPen = false;
    buttonStateOff('eraserButton');
    optionEraser = false;
    buttonStateOff('colourPickerButton');
    optionColourSelecting = false;
    buttonStateOff('floodButton');
    optionFlooding = false;
    buttonStateOff('highlightButton');
    buttonStateOff('cutButton');
    optionCut = false;
}

function cutStatus() {
    buttonStateOff('drawButton');
    optionPen = false;
    buttonStateOff('eraserButton');
    optionEraser = false;
    buttonStateOff('colourPickerButton');
    optionColourSelecting = false;
    buttonStateOff('floodButton');
    optionFlooding = false;
    buttonStateOff('highlightButton');
    buttonStateOff('stampButton');
    optionStamp = false;
}

function buttonStateOn(buttonID) {
    console.log(buttonID + " on")
    buttonID.checked = true;
    document.getElementById(buttonID).style.background = "#4fd443";
    document.getElementById(buttonID).style.color = "#353535";
}

function buttonStateOff(buttonID) {
    console.log(buttonID + " off")
    buttonID.checked = false;
    document.body.style.cursor = 'auto';
    document.getElementById(buttonID).style.background = "#353535";
    document.getElementById(buttonID).style.color = "azure";
}
