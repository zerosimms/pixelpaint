var capture;
var videoPlay = false;


function startCamera() {
    console.log("Starting Camera");
    videoPlay = true;
    capture = createCapture(VIDEO);
    capture.hide();
    resizeCanvas(320, 320);
    exportImage.resizeCanvas(320, 320);
    pixelSizeField.value = 40;
    document.getElementById('canvasWidth').value = 320;
    document.getElementById('canvasHeight').value = 320;
    document.getElementById('exportWidth').value = 320;
    document.getElementById('exportHeight').value = 320;
}

function stopCamera() {
    console.log("Stopped Camera");
    capture.stop();
    videoPlay = false;
    capture.loadPixels();
    for (var y = 0; y < height; y += pixelSize) {
        for (var x = 0; x < width; x += pixelSize) {
            var offset = ((y * width) + x) * 8;
            exportImage.fill(capture.pixels[offset],
                capture.pixels[offset + 1],
                capture.pixels[offset + 2]);
            exportImage.rect(x, y, pixelSize, pixelSize);
        }
    }
    exportImage.updatePixels();
}

function cameraRunning() {
    capture.loadPixels();
    for (var y = 0; y < height; y += pixelSize) {
        for (var x = 0; x < width; x += pixelSize) {
            var offset = ((y * width) + x) * 8;
            noStroke();
            fill(capture.pixels[offset],
                capture.pixels[offset + 1],
                capture.pixels[offset + 2]);
            rect(x, y, pixelSize, pixelSize);
        }
    }
    capture.updatePixels();
}