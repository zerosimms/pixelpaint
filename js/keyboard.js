function keyPressed() {
    //'D' - Select Pen
    if (keyCode === 68) {
        if (!optionPen) {
            optionPen = true;
            penStatus();
        } else {
            optionPen = false;
            penStatus();
        }
    }

    //'E' - Select Eraser
    if (keyCode === 69) {
        if (!optionEraser) {
            optionEraser = true;
            eraserStatus();
        } else {
            optionEraser = false;
            eraserStatus();
        }
    }

    //'F' - Flood
    if (keyCode === 70) {
        if (!optionFlooding) {
            optionFlooding = true;
            floodStatus();
        } else {
            optionFlooding = false;
            floodStatus();
        }
    }

    //'G' - Capture the selected Colour
    if (keyCode === 71) {
        if (!optionColourSelecting) {
            optionColourSelecting = true;
            colourPickerStatus();
        } else {
            optionColourSelecting = false;
            colourPickerStatus();
        }
    }

    //'P' - Pen Colour
    if (keyCode === 80) {
        document.getElementById('colorBar').click();
    }

    //'C' - Show Cells
    if (keyCode === 67) {
        if (!spriteSheetVisibile) {
            console.log("Sprite sheet cells: " + cellCount);
            spriteSheetVisibile = true;
            spriteSheetButton = true;
            spriteSheetButton.checked = true;
            buttonStateOn('spriteSheetButton');
            drawSpritesheetTemplate();
        } else {
            spriteSheetVisibile = false;
            spriteSheetButton.checked = false;
            buttonStateOff('spriteSheetButton');
            spriteSheet.clear();
        }
    }
    //'S' - Highlight Selection
    if (keyCode === 83) {
        selectStatus();
        const highlightButton = document.getElementById('highlightButton');
        highlighterSelected = true;

        if (!selectingArea && !selectedArea && highlighterSelected) {
            originalMouseX = mouseX - (mouseX % pixelSize);
            originalMouseY = mouseY - (mouseY % pixelSize);
            console.log("1 Area Not Selecting and not selected")
            highlightButton.checked = true;
            selectingArea = true;
            buttonStateOn('highlightButton');
            
        } else if (highlighterSelected && selectingArea) {
            console.log("2 Area selecting and selected ");

            const highlightButton = document.getElementById('highlightButton');
            highlighterSelected = false;
            selectingArea = false;
            selectedArea = false
            highlight.clear();
            buttonStateOff('highlightButton');
            highlightButton.checked = false;
            stampImage = exportImage.get(originalMouseX, originalMouseY, distanceXShape, distanceYShape);
        }
    }
}