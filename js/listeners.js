var darkenFilterSelected = false;
var overlayFilterSelected = false;
var hardlightSelected = false;

function listening() {
    pixelSize = width / pixelSizeField.value;
    gridSize = width / gridSizeField.value;
    multiX = mouseX - (mouseX % pixelSize);
    multiY = mouseY - (mouseY % pixelSize);

    if (videoPlay) {
        cameraRunning();
    }

    if (!videoPlay) {
        image(gridImage, 0, 0);
        copy(exportImage, 0, 0, width, height, 0, 0, width, height);
        image(highlight, 0, 0);
        image(spriteSheet, 0, 0);
    }

    if (optionStamp) {
        image(stampImage, multiX, multiY);
    }

    if (optionCut) {
        noStroke();
        fill(255, 50, 50, 50);
        rect(multiX, multiY, selectWidth, selectHeight);
    }

    if (colourSelected) {
        buttonStateOn('drawButton');
        optionPen = true;
        buttonStateOff('colourPickerButton');
        optionColourSelecting = false;
        colourSelected = false;
    }


    if (!optionColourSelecting || !optionFlooding) {
        image(mousePointer, multiX, multiY, pixelSize, pixelSize);
    }
}