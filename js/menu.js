dragElement(document.getElementById("canvasArea"));
dragElement(document.getElementById("paintOptions"));
dragElement(document.getElementById("document"));
dragElement(document.getElementById("export"));
dragElement(document.getElementById("gif"));
dragElement(document.getElementById("brushOptions"));
dragElement(document.getElementById("filterOptions"));

document.getElementById("canvasArea").style.top = getCookie("canvasAreaTop");
document.getElementById("canvasArea").style.left = getCookie("canvasAreaLeft");
document.getElementById("paintOptions").style.top = getCookie("paintOptionsTop");
document.getElementById("paintOptions").style.left = getCookie("paintOptionsLeft");
document.getElementById("document").style.top = getCookie("documentTop");
document.getElementById("document").style.left = getCookie("documentLeft");
document.getElementById("export").style.top = getCookie("exportTop");
document.getElementById("export").style.left = getCookie("exportLeft");
document.getElementById("gif").style.top = getCookie("gifTop");
document.getElementById("gif").style.left = getCookie("gifLeft");
document.getElementById("brushOptions").style.top = getCookie("brushOptionsTop");
document.getElementById("brushOptions").style.left = getCookie("brushOptionsLeft");
document.getElementById("filterOptions").style.top = getCookie("filterOptionsTop");
document.getElementById("filterOptions").style.left = getCookie("filterOptionsLeft");
    
function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

function saveTopPosition(menuItem, top) {
    document.cookie = menuItem + "Top=" + top;
}

function saveLeftPosition(menuItem, left) {
    document.cookie = menuItem + "Left=" + left;
}

function dragElement(elmnt) {
    if (document.getElementById(elmnt.id + "Header")) {
        document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
    } else {
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        saveTopPosition(elmnt.id, elmnt.style.top);
        saveLeftPosition(elmnt.id, elmnt.style.left);
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}