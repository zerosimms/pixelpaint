var xPos, yPos;
var selectingArea = false;
var selectedArea = false;

function mouseReleased() {
    document.body.style.cursor = 'auto';
    if (mouseX > 0 && mouseY > 0) {
        saveHistory(historyCounter);
        historyCounter++;
    }
}

function mousePressed() {
    const colorInput = document.getElementById('colorBar');
    if (mouseX > 0 && mouseY > 0) {
        lastStateNumber++;
    }

    if (optionPen) {
        document.body.style.cursor = 'crosshair';
        exportImage.fill(colorInput.value);
        exportImage.noStroke();
        exportImage.rect(multiX, multiY, pixelSize, pixelSize);
    }

    if (highlighterSelected && !selectingArea && !selectedArea) {
        console.log("1 Area Not Selecting and not selected")
        highlighterSelected = true;
        originalMouseX = mouseX - (mouseX % pixelSize);
        originalMouseY = mouseY - (mouseY % pixelSize);
        selectingArea = true;
    }

    else if (selectingArea && !selectedArea) {
        console.log("2 Area selecting and selected ");
        const highlightButton = document.getElementById('highlightButton');
        stampImage = exportImage.get(originalMouseX, originalMouseY, distanceXShape, distanceYShape);
        highlighterSelected = false;
        selectingArea = false;
        selectedArea = false
        selectWidth = dist(originalMouseX, 0, multiX, 0);
        selectHeight = dist(0, originalMouseY, 0, multiY);
        highlight.clear();
        buttonStateOff('highlightButton');
        highlightButton.checked = false;
    }

    if (optionCut) {
        console.log("cut width: " + selectWidth);
        console.log("cut Height: " + selectHeight);
        exportImage.drawingContext.clearRect(multiX, multiY, selectWidth, selectHeight);
    }

    if (optionStamp) {
        console.log("Stamping");
        //Redraw the image dont copy it and dont forget about dragging
        exportImage.image(stampImage, multiX, multiY);
        saveHistory(historyCounter);
        historyCounter++;
    }

    if (optionFlooding) {
        console.log("Flooding");
        floodFillCanvas();
    }

    if (darkenFilterSelected) {
        randomHeight = random(pixelSize, pixelSize + 100)
        blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize, randomHeight, DARKEST);
        exportImage.blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize, randomHeight, DARKEST);
    }

    if (overlayFilterSelected) {
        blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, MULTIPLY);
        exportImage.blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, MULTIPLY);
    }

    if (hardlightSelected) {
        blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, SCREEN);
        exportImage.blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, SCREEN);
    }

    else if (!colourSelected && optionColourSelecting) {
        colourSelected = true;
        optionColourSelecting = false;
    }

    else if (colourSelected && !optionColourSelecting) {
        colourSelected = false;
        optionColourSelecting = false;
    }
}

function mouseDragged() {
    if (optionPen) {
        if (!optionFlooding || !floodAreaSelected) {
            document.body.style.cursor = 'crosshair';
            var colorInput = document.getElementById('colorBar');
            exportImage.fill(colorInput.value);
            exportImage.noStroke();
            exportImage.rect(multiX, multiY, pixelSize, pixelSize);
        }
    } else if (optionEraser) {
        myErase();
    }

    else if (optionStamp) {
        console.log("Stamping");
        exportImage.image(stampImage, multiX, multiY);
    }

    if (darkenFilterSelected) {
        randomHeight = random(pixelSize, pixelSize + 100)
        blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize, randomHeight, DARKEST);
        exportImage.blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize, randomHeight, DARKEST);
    }

    if (overlayFilterSelected) {
        blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, MULTIPLY);
        exportImage.blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, MULTIPLY);
    }

    if (hardlightSelected) {
        blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, SCREEN);
        exportImage.blend(multiX, multiY, pixelSize, pixelSize, multiX, multiY, pixelSize + 10, pixelSize + 10, SCREEN);
    }
}

function myErase() {
    if (!optionColourSelecting) {
        multiX = mouseX - (mouseX % pixelSize);
        multiY = mouseY - (mouseY % pixelSize);
        exportImage.drawingContext.clearRect(multiX, multiY, pixelSize, pixelSize);
    }
}
