var storedImaged = [];
var savedImageString = [];
var fileSelected = document.getElementById('openFileButton');

function loadState() {
    console.log("Loading local copy");
    storedImaged = JSON.parse(localStorage.getItem("lastSave"));
    document.getElementById('fileNameField').value = storedImaged[0];
    document.getElementById('pixelSizeField').value = storedImaged[3];
    document.getElementById('canvasWidth').value = storedImaged[1];
    document.getElementById('canvasHeight').value = storedImaged[2];
    document.getElementById('exportWidth').value = storedImaged[1];
    document.getElementById('exportHeight').value = storedImaged[2];
    pixelSize = storedImaged[3];
    resizeCanvas(storedImaged[1], storedImaged[2]);
    exportImage.resizeCanvas(storedImaged[1], storedImaged[2]);
    exportImage.resizeCanvas(storedImaged[1], storedImaged[2]);
    loadPixelImage(pixelSize);
}

function loadPixelImage(thePixelSize) {
    let index = 4;
    for (var y = 0; y < height; y += thePixelSize) {
        for (var x = 0; x < width; x += thePixelSize) {
            noStroke();
            fill(storedImaged[index], storedImaged[index], storedImaged[index]);
            rect(x, y, thePixelSize, thePixelSize);
            exportImage.noStroke();
            exportImage.fill(storedImaged[index], storedImaged[index], storedImaged[index]);
            exportImage.rect(x, y, thePixelSize, thePixelSize);
            index++;
        }
    }
}

fileSelected.addEventListener('change', function (e) {
    var fileTobeRead = fileSelected.files[0];
    var fileExtension = fileTobeRead.name.replace(/^.*\./, '');

    if (fileExtension === "ids" || 'txt') {
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            console.log("Opened File");
            savedImageString = fileReader.result;
            localStorage.setItem("lastSave", savedImageString);
            loadState();
        }
        fileReader.readAsText(fileTobeRead);
    }
    else {
        alert("Please select a 'ids' or 'txt' file");
    }
}, false);

function savePixelImage(filename) {
    console.log("Filename: " + filename + '.ids');
    savedImageString = [];
    let index = 4;
    savedImageString[0] = filename;
    savedImageString[1] = document.getElementById('canvasWidth').value;
    savedImageString[2] = document.getElementById('canvasHeight').value;
    savedImageString[3] = document.getElementById('pixelSizeField').value;
    for (let y = pixelSize / 2; y < height; y += pixelSize) {
        for (let x = pixelSize / 2; x < width; x += pixelSize) {
            savedImageString[index++] = exportImage.get(x, y);
            localStorage.setItem("lastSave", JSON.stringify(savedImageString));
        }
    }
    exportFile();
}

function quickSave(filename) {
    console.log("Filename: " + filename);
    let index = 4;
    savedImageString[0] = filename;
    savedImageString[1] = document.getElementById('canvasWidth').value;
    savedImageString[2] = document.getElementById('canvasHeight').value;
    savedImageString[3] = document.getElementById('pixelSizeField').value;
    for (let y = pixelSize / 2; y < height; y += pixelSize) {
        for (let x = pixelSize / 2; x < width; x += pixelSize) {
            savedImageString[index++] = get(x, y);
            localStorage.setItem("lastSave", JSON.stringify(savedImageString));
        }
    }
    savedToast();
}

function exportFile() {
    var file = new Blob([JSON.stringify(savedImageString)], { type: 'ids' });
    var a = document.createElement("a"), url = URL.createObjectURL(file);
    a.href = url;
    a.download = document.getElementById('fileNameField').value;
    document.body.appendChild(a);
    a.click();

    setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
    }, 0);
}

function savedToast() {
    var x = document.getElementById("savedToast");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
    console.log("Saved");
}