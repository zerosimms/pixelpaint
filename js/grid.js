function resizeGrid() {
    resizeCanvas(canvasWidth, canvasHeight);
    exportImage = createGraphics(canvasWidth, canvasHeight);
    clear();
    reDrawGrid();
}

function reDrawGrid() {
    gridImage.clear();
    gridImage = createGraphics(width, height);
    console.log("Redrawing Grid");
    for (var i = 0; i < width; i++) {
        for (var j = 0; j < height; j++) {
            if ((i + j) % 2 == 0) {
                gridImage.noStroke();
                gridImage.fill(239, 239, 239);
            } else {
                gridImage.noStroke();
                gridImage.fill(225, 225, 225);
            }
            gridImage.rect(i * gridSize, j * gridSize, gridSize, gridSize);
        }
    }
}

function drawSpritesheetTemplate() {
    spriteSheet.clear();
    spriteSheet = createGraphics(width, height);

    for (var x = 0; x < width; x += width / spritesheetField.value) {
        for (var y = 0; y < height; y += height / spritesheetField.value) {
            spriteSheet.stroke(255, 0, 0);
            spriteSheet.strokeWeight(1);
            spriteSheet.line(x, 0, x, height);
            spriteSheet.line(0, y, width, y);
        }
    }
}